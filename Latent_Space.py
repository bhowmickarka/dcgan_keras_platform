# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 11:26:04 2019

@author: Arka_Thesis
"""

"""
Load Model and Generate Faces. In this part of the code we are going to Load 
a suitable model and gnerate the faces
"""
import numpy as np
from numpy import asarray
from numpy.random import randn
from numpy.random import randint
from keras.models import load_model
from matplotlib import pyplot

#Generating the latent space
def generate_latent_space(latent_dim, n_samples,n_classes=10):
    x_input=randn(latent_dim*n_samples)
    X_fake=x_input.reshape((n_samples,latent_dim))
    
    return X_fake

#def show_generated_faces(examples,n):
    # create a plot of generated images
def plot_generated(examples, n):
	# plot images
	for i in range(n):
		# define subplot
		pyplot.subplot(1, n, 1 + i)
		# turn off axis
		pyplot.axis('off')
		# plot raw pixel data
		pyplot.imshow(examples[i, :, :])
	pyplot.show()

"""
Interpolate  betwee generated faces. 
This part of the code is used to modify faces that we have generated
"""
def interpolating_points(p1,p2,num=10):
    ratios=np.linspace(0,1,num=num)
    vectors=list()
    for ratio in ratios:
        v=(1-ratio)*p1+ratio*p2
        vectors.append(v)
    return(asarray(vectors))
    
    
#Load the model
model=load_model('generator_model_004.h5')
#Generate latent_points
latent_points=generate_latent_space(100,2)

print(latent_points.shape)
#Generate images
interpolated=interpolating_points(latent_points[0],latent_points[1])
X=model.predict(interpolated)
#Scale the images
X=(X+1)/2.0
#Plot the result
plot_generated(X,len(interpolated))


"""
_______________________________________________________________________________
The 2nd half of the code will will deal with Vector Aritmatic on Faces
"""
#def average_points(points,ix):
    #zero_ix=[i-1 for i in ix]

                                       
    
    
