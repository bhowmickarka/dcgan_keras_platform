# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 11:28:06 2019

@author: Arka_Thesis

This script is used to  load the dataset and convert it into a numpy array
"""

from PIL import Image
from matplotlib import pyplot
import numpy as np
from os import listdir
#from mtcnn.mtcnn import MTCNN
#import cv2
#load an image as an rgb numpy array
def load_image(filename):
    #Load image from the file
    image=Image.open(filename)
    #convert to arraay
    pixels=np.asarray(image)
    pixels1=np.resize(pixels,(80,80,3))
    return pixels1


#load images and extract faces of all images in a array
def load_faces(directory, n_faces):
    faces=list()
    
    for filename in listdir(directory):
        #load the image
        pixels=load_image(directory+filename)
        
        faces.append(pixels)
        
        if(len(faces)>=n_faces):
            break
    return np.asarray(faces)

#plot a list of loaded faces
    
def plot_faces(faces, n):
	for i in range(n * n):
		# define subplot
		pyplot.subplot(n, n, 1 + i)
		# turn off axis
		pyplot.axis('off')
		# plot raw pixel data
		pyplot.imshow(faces[i])
	pyplot.show()
    
# directory that contains all images
directory = 'Dataset/'
# load and extract all faces
faces = load_faces(directory, 2999)
print('Loaded: ', faces.shape)
# plot faces
np.savez_compressed('img_align_celeba.npz', faces)