# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 12:33:51 2019

@author: Arka_Thesis


Note:- This script defines the general GAN most basic structure
"""
from numpy import load
from numpy import zeros
from numpy import ones
from numpy.random import randn
from numpy.random import randint
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Reshape
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Dropout
from matplotlib import pyplot

def discriminator(in_shape=(80,80,3)):
    model=Sequential()
    
    model.add(Conv2D(128,(5,5),padding='same',input_shape=in_shape))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Conv2D(128,(5,5),strides=(2,2),padding='same'))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Conv2D(128,(5,5),strides=(2,2),padding='same'))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Conv2D(128,(5,5),strides=(2,2),padding='same'))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Conv2D(128,(5,5),strides=(2,2),padding='same'))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Flatten())
    model.add(Dropout(0.4))
    model.add(Dense(1,activation='sigmoid'))
    
    #Compiling
    opt=Adam(lr=0.0002, beta_1=0.5)
    model.compile(loss='binary_crossentropy', optimizer=opt,metrics=['accuracy'])
    return model



#Defining the generator
def generator(latent_dim):
    model=Sequential()
    
    #foundation for 5x5 feature maps
    n_nodes=128*5*5
    model.add(Dense(n_nodes,input_dim=latent_dim))
    model.add(LeakyReLU(alpha=0.2))
    model.add(Reshape((5,5,128)))
    # upsample to 10x10
    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))
	# upsample to 20x20
    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))
	# upsample to 40x40
    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))
	# upsample to 80x80
    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))
	# output layer 80x80x3
    model.add(Conv2D(3, (5,5), activation='tanh', padding='same'))
    return model
    
# define the combined generator and discriminator model, for updating the generator
def define_gan(g_model, d_model):
	# make weights in the discriminator not trainable
	d_model.trainable = False #The discriminator is trained separately
	# connect them
	model = Sequential()
	# add generator
	model.add(g_model)
	# add the discriminator
	model.add(d_model)
	# compile model
	opt = Adam(lr=0.0002, beta_1=0.5)
	model.compile(loss='binary_crossentropy', optimizer=opt)
	return model


	
# load and prepare training images 
def load_real_samples():
	# load the face dataset
	data = load('img_align_celeba.npz')
	X = data['arr_0']
	# convert from unsigned ints to floats
	X = X.astype('float32')
	# scale from [0,255] to [-1,1]
	X = (X - 127.5) / 127.5
	return X
    
data=load_real_samples()
print(data[1].shape)

'''
#The generate_real_samples() function below implements this, 
#taking the prepared dataset as an argument, 
#selecting and returning a random sample of face images and their corresponding class label for the discriminator, 
#specifically class=1, indicating that they are real images.
'''	
# select real samples
def generate_real_samples(dataset, n_samples):
	# choose random instances
	ix = randint(0, dataset.shape[0], n_samples)
	# retrieve selected images
	X = dataset[ix]
	# generate 'real' class labels (1)
	y = ones((n_samples, 1))
	return X, y

#Generate latent_space_points() from Gaussian distributed Random Variables
def generate_latent_points(latent_dim, n_samples):
	# generate points in the latent space
	x_input = randn(latent_dim * n_samples)
	# reshape into a batch of inputs for the network
	x_input = x_input.reshape(n_samples, latent_dim)
	return x_input

'''
#The generate_fake_samples() function below implements this, 
#taking the generator model and size of the latent space as arguments, 
#then generating points in the latent space and using them as input to the generator model. 
#The function returns the generated images and their corresponding class label for the discriminator model, 
#specifically class=0 to indicate they are fake or generated.
'''    

	
# use the generator to generate n fake examples, with class labels
def generate_fake_samples(g_model, latent_dim, n_samples):
	# generate points in latent space
	x_input = generate_latent_points(latent_dim, n_samples)
	# predict outputs
	X = g_model.predict(x_input)
	# create 'fake' class labels (0)
	y = zeros((n_samples, 1))
	return X, y



#Training the generator and the discriminator
def train(gmodel,dmodel,gan_model,dataset,latent_dim,n_epochs=4,n_batch=128):
    bat_per_epo = int(dataset.shape[0]/n_batch)
    half_batch = int(n_batch/2)
    
    for i in range(n_epochs):
        
        for j in range(bat_per_epo):
            #Get the real samples
            X_real,Y_real=generate_real_samples(dataset, half_batch)
            #Update discrimator weight
            d_loss1,_=dmodel.train_on_batch(X_real,Y_real)
        
            # generate 'fake' examples
            X_fake,y_fake=generate_fake_samples(gmodel,latent_dim,half_batch)
        
            #Update the discriminator model weights
            d_loss2,_=dmodel.train_on_batch(X_fake,y_fake)
        
            # prepare points in latent space as input for the generator
            X_gan= generate_latent_points(latent_dim, n_batch)
		    # create inverted labels for the fake samples
            y_gan= ones((n_batch,1))
		    # update the generator via the discriminator's error
            g_loss= gan_model.train_on_batch(X_gan,y_gan)
        
            # summarize loss on this batch
		  # print('>%d, %d/%d, d1=%.3f, d2=%.3f g=%.3f'%i+1,j+1,bat_per_epo,d_loss1,d_loss2,g_loss))
            print('Epoch:',i+1,'Batch:',j+1,'Batch_per_epoch:',bat_per_epo,'DLoss1:',d_loss1,'DLoss2:',d_loss2,'Generator_loss:',g_loss)
            if((i+1)%2==0):
                print('Now I want to summarize the Performance for you')
                summarize_performance(i,g_model,d_model,dataset,latent_dim)
                
            
			    
            
    #if (i+1) % 10 == 0:
        #summarize_performance(i, g_model, d_model, dataset, latent_dim)
        
#The summarize_performance function generates samples 
#and evaluates the performance of the discriminator on real and fake samples


#Create and save a plot for generated Images

def save_plot(examples, epoch, n=10):
	# scale from [-1,1] to [0,1]
	examples = (examples + 1) / 2.0
	# plot images
	for i in range(n * n):
		# define subplot
		pyplot.subplot(n, n, 1 + i)
		# turn off axis
		pyplot.axis('off')
		# plot raw pixel data
		pyplot.imshow(examples[i])
	# save plot to file
	filename = 'generated_plot_e%03d.png' % (epoch+1)
	pyplot.savefig(filename)
	pyplot.close()
'''    
evaluate the discriminator, plot generated images, save generator model_____________________
'''
def summarize_performance(epoch, g_model, d_model, dataset, latent_dim, n_samples=100):
    print('I have entered summarize_performance')
	# prepare real samples
	X_real, y_real = generate_real_samples(dataset, n_samples)
	# evaluate discriminator on real examples
	_, acc_real = d_model.evaluate(X_real, y_real, verbose=0)
	# prepare fake examples
	x_fake, y_fake = generate_fake_samples(g_model, latent_dim, n_samples)
	# evaluate discriminator on fake examples
	_, acc_fake = d_model.evaluate(x_fake, y_fake, verbose=0)
	# summarize discriminator performance
	print('>Accuracy real: %.0f%%, fake: %.0f%%' % (acc_real*100, acc_fake*100))
	# save plot
	save_plot(x_fake, epoch)
	# save the generator model tile file
	filename = 'generator_model_%03d.h5' % (epoch+1)
	g_model.save(filename)
    

latent_dim=100
d_model=discriminator()
g_model=generator(latent_dim)
gan_model=define_gan(g_model,d_model)
dataset=load_real_samples()
train(g_model,d_model,gan_model,dataset,latent_dim)
